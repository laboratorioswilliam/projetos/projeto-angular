import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AlunoRoutingModule } from './aluno-routing.module';
import { AlunoComponent } from './aluno.component';
import { MaterialModule } from '../shared/material/material.module';


@NgModule({
  declarations: [AlunoComponent],
  imports: [
    CommonModule,
    AlunoRoutingModule,
    MaterialModule
  ]
})
export class AlunoModule { }
