import { HomeComponent } from './home/home.component';
import { MaterialModule } from './shared/material/material.module';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MinhaNavComponent } from './minha-nav/minha-nav.component';
import { LayoutModule } from '@angular/cdk/layout';
import { DashboardComponent } from './dashboard/dashboard.component';
import { MinhatabelaComponent } from './minhatabela/minhatabela.component';
import { DialogBoxComponent } from './home/dialog-box/dialog-box.component';
import { FormsModule } from '@angular/forms';
import { TurmaComponent } from './turma/turma.component';
import { HttpClientModule } from '@angular/common/http';


@NgModule({
  declarations: [
    AppComponent,
    MinhaNavComponent,
    DashboardComponent,
    MinhatabelaComponent,
    HomeComponent,
    DialogBoxComponent,
    TurmaComponent

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    LayoutModule,
    MaterialModule,
    FormsModule,
    HttpClientModule




  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
