import { DataSource } from '@angular/cdk/collections';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { map } from 'rxjs/operators';
import { Observable, of as observableOf, merge } from 'rxjs';

export interface TesteItem {
  name: string;
  gender: string;
  company: string;
  age: number;

}
/**
 * Data source for the Minhatabela view. This class should
 * encapsulate all logic for fetching and manipulating the displayed data
 * (including sorting, pagination, and filtering).
 */
export class ProfessorDataSource extends DataSource<TesteItem> {
  dadosDeExemploWilliam: TesteItem [];
  paginator: MatPaginator;
  sort: MatSort;


  constructor() {
    super();
    this.buscarDados((data) => {
      this.dadosDeExemploWilliam = data;
    });

    console.log(this.dadosDeExemploWilliam);
  }

  buscarDados(cb) {
    const requisicao = new XMLHttpRequest();
    requisicao.open('GET', `http://localhost:3000/pessoas`);
    // requisicao.open('GET', `http://swimlane.github.io/ngx-datatable/assets/data/company.json`);

    requisicao.onload = () => {
      const dadosRecebidos = JSON.parse(requisicao.response);
      cb(dadosRecebidos);
    };
    requisicao.send();
  }



  /**
   * Connect this data source to the table. The table will only update when
   * the returned stream emits new items.
   * @returns A stream of the items to be rendered.
   */
  connect(): Observable<TesteItem[]> {
    // Combine everything that affects the rendered data into one update
    // stream for the data-table to consume.
    const dataMutations = [
      observableOf(this.dadosDeExemploWilliam),

      this.paginator.page,
      this.sort.sortChange
    ];

    return merge(...dataMutations).pipe(map(() => {
      return this.getPagedData(this.getSortedData([...this.dadosDeExemploWilliam]));
    }));
  }

  /**
   *  Called when the table is being destroyed. Use this function, to clean up
   * any open connections or free any held resources that were set up during connect.
   */
  disconnect() {}

  /**
   * Paginate the data (client-side). If you're using server-side pagination,
   * this would be replaced by requesting the appropriate data from the server.
   */
  private getPagedData(data: TesteItem[]) {
    const startIndex = this.paginator.pageIndex * this.paginator.pageSize;
    return data.splice(startIndex, this.paginator.pageSize);
  }

  /**
   * Sort the data (client-side). If you're using server-side sorting,
   * this would be replaced by requesting the appropriate data from the server.
   */
  private getSortedData(data: TesteItem[]) {
    if (!this.sort.active || this.sort.direction === '') {
      return data;
    }

    return data.sort((a, b) => {
      const isAsc = this.sort.direction === 'asc';
      switch (this.sort.active) {
        case 'name': return compare(a.name, b.name, isAsc);
        case 'gender': return compare(+a.gender, +b.gender, isAsc);
        default: return 0;
      }
    });
  }
}

/** Simple sort comparator for example ID/Name columns (for client-side sorting). */
function compare(a: string | number, b: string | number, isAsc: boolean) {
  return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
}
