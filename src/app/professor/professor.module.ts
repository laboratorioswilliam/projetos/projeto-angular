import { MaterialModule } from '../shared/material/material.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProfessorRoutingModule } from './professor-routing.module';
import { ProfessorComponent } from './professor.component';


@NgModule({
  declarations: [ProfessorComponent],
  imports: [
    CommonModule,
    ProfessorRoutingModule,
    MaterialModule
  ]
})
export class ProfessorModule { }
