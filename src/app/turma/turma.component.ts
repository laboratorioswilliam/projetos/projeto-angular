import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';



@Component({
  selector: 'app-turma',
  templateUrl: './turma.component.html',
  styleUrls: ['./turma.component.scss']
})
export class TurmaComponent implements OnInit {

displayedColumns: string[] = [ 'nome', 'matricula','semestre','acao'];
dataSource: any;
private API_URL = 'https://unidesc-api.herokuapp.com/v1/api/alunos';

  productForm: FormGroup;
  isLoadingResults = false;
  constructor(
    // private router: Router,
    // private formBuilder: FormBuilder,
    private http:HttpClient) { }


  ngOnInit(): void {
    this.buscarAlunos();
  }


  private buscarAlunos() {
    this.http.get(this.API_URL)
      .subscribe(res => {
        this.dataSource = res["alunos"];
        console.log(this.dataSource);
        this.isLoadingResults = false;
      });
  }
}
